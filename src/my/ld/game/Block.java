package my.ld.game;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Block {
	
	private MyGame game;
	private Rectangle hitbox;
	private Texture border;
	private float X;
	private float Y;
	private ArrayList<String> messages;
	
	public Block(MyGame game, Texture texture, float x, float y)
	{
		this.game = game;
		this.hitbox = new Rectangle(x, y, this.game.UNIT, this.game.UNIT);
		this.X = x;
		this.Y = y;
		this.border = texture;
	}
	
	public Rectangle getHitbox()
	{
		return this.hitbox;
	}
	
	public void update()
	{
		
	}
	
	public void draw(SpriteBatch sb)
	{
		sb.draw(this.border, this.X, this.Y, game.UNIT, game.UNIT);
	}
	
}
