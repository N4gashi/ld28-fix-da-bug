package my.ld.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Player {
	
	
	private int x;
	private int y;
	private int x_unit;
	private int y_unit;
	private int num_case;
	private int speed = 200;
	private int gravity = 250;
	
	private int offSetX;
	private int offSetY;
	
	private int currCaseX;
	private int currLineY;
	
	private boolean jump;
	private int currPosY;
	private boolean fall;
	private Texture pxl, red;
	private Rectangle hitbox;
	private MyGame game;
	private Level level;
	private ArrayList<Block> blocks;
	private int num_animation = 0;
	
	private boolean ismove = false;
	private int animation = 0;
	
	public Player(MyGame game, Level level){
		this.game = game;
		this.x = game.UNIT * 5;
		this.y = game.UNIT * 25;
		this.x_unit = 5;
		this.y_unit = 25;
		this.offSetX = this.x;
		this.offSetY = this.y;
		this.hitbox = new Rectangle((float)this.offSetX, (float)this.offSetY, (float)game.UNIT*2, (float)this.game.UNIT*2);
		this.pxl = new Texture(Gdx.files.internal("assets/pixel2.png"));
		this.jump = false;
		this.currPosY = 0;
		this.fall = false;
		this.num_case = 0;
		this.level = level;
		this.blocks = level.getBlocks();
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSpeed() {
		return speed;
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getOffSetX() {
		return offSetX;
	}

	public void setOffSetX(int offSetX) {
		this.offSetX = offSetX;
	}

	public int getOffSetY() {
		return offSetY;
	}

	public void setOffSetY(int offSetY) {
		this.offSetY = offSetY;
	}

	public Rectangle getHitbox()
	{
		return this.hitbox;
	}
	
	public boolean isCollisionX(ArrayList<Block> collisionRect, int newX)
	{
		
		boolean boolvalue = false;
		
		for(Block block : collisionRect)
		{
			Rectangle previsionalPosition = new Rectangle(newX, this.offSetY, this.game.UNIT, this.game.UNIT);
			
            if (block.getHitbox().overlaps(previsionalPosition))
            {
            	boolvalue = true;
            	break;
            }
        }
		
		return boolvalue;
	}

	public boolean isCollisionY(ArrayList<Block> collisionRect, int newY)
	{
		
		boolean boolvalue = false;
		
		for(Block block : collisionRect)
		{
			Rectangle previsionalPosition = new Rectangle(this.offSetX, newY, this.game.UNIT, this.game.UNIT);
			
            if (block.getHitbox().overlaps(previsionalPosition))
            {
            	boolvalue = true;
            	break;
            }
        }
		
		return boolvalue;
	}

	public int typeOfHoveredBlock(int[][] data)
	{
		int xAxis = this.offSetX;
		int yAxis = this.y;
		this.currCaseX = (xAxis <= this.game.UNIT) ? 0 : xAxis / game.UNIT;
		this.currLineY = (yAxis <= this.game.UNIT) ? 0 : yAxis / game.UNIT;
		
		//int type = data[currLineY][currCaseX];
		
		return 0;
	}
	
	public Texture getPxl() {
		return pxl;
	}

	public void setPxl(Texture pxl) {
		this.pxl = pxl;
	}
	
	public void moveLeft()
	{
		ismove = true; // permettant l'incrémentation de l'animation du sprite du pxl
		
		//if((this.offSetX > this.game.UNIT) || ((level.getData()[this.num_case - 1] == 0) && (level.getData()[this.num_case - 2] == 0)))
		if(this.offSetX > this.game.UNIT)
		{
			int da_newX = Math.round(this.offSetX - this.speed * Gdx.graphics.getDeltaTime());
			if(!this.isCollisionX(this.blocks, da_newX))
			{
				//System.out.println("pas de collision left ! ");
				this.offSetX -= this.speed * Gdx.graphics.getDeltaTime();
				float newX = this.hitbox.getX() - (this.speed * Gdx.graphics.getDeltaTime());
				this.hitbox.setX(newX);
			}
			else
			{
				System.out.println("collision left ! ");
			}
		}
	}
	
	public void moveRight()
	{
		ismove = true;
		
		//if((this.offSetX < game.RES_WIDTH - this.game.UNIT) ||((level.getData()[this.num_case -1] == 0) &&(level.getData()[this.num_case -2] == 0)))
		if(this.offSetX < game.RES_WIDTH - this.game.UNIT)
		{
			int da_newX = Math.round(this.offSetX + this.speed * Gdx.graphics.getDeltaTime());
			if(!this.isCollisionX(this.blocks, da_newX))
			{
				//System.out.println("pas de collision left ! ");
				this.offSetX += this.speed * Gdx.graphics.getDeltaTime();
				float newX = this.hitbox.getX() + (this.speed * Gdx.graphics.getDeltaTime());
				this.hitbox.setX(newX);
			}
			else
			{
				System.out.println("collision right ! ");
			}
		}
		
	}
	
	public void moveJump()
	{
		
		if(!this.jump && !this.fall)
		{
			this.jump = true;
			this.currPosY = this.offSetY;
		}
		/*
		
		if(this.nbJump < 2)
		{
			int timeElapsed = 0;
			nbJump++;
			
			while(timeElapsed < 1500)
			{
				this.offSetY += this.gravity *1.5* Gdx.graphics.getDeltaTime();
				
				float newY = this.hitbox.getY() + (this.gravity *2* Gdx.graphics.getDeltaTime());
				this.hitbox.setY(newY);
				
				timeElapsed += Gdx.graphics.getDeltaTime();
				
			}
			
		}*/
		
	}
	
	public void doJump()
	{
		
	}
	
	public void moveFall()
	{
		if(this.jump)
		{
			// entre le sol est en haut du saut
			
			if((!this.fall) && ((this.offSetY - this.currPosY) < (this.game.UNIT*3)))
			{
				int newY = Math.round(this.offSetY + this.gravity *2* Gdx.graphics.getDeltaTime());
				
				if(!isCollisionY(this.blocks, newY))
				{
					this.offSetY += this.gravity *2* Gdx.graphics.getDeltaTime();
					this.y += this.gravity *2* Gdx.graphics.getDeltaTime();
					this.speed = 250;
				}
				else
				{
					this.fall = true;
				}
			}
			else // si il arrive en haut
			{ 
				this.speed = 200;
				this.fall = true;
				
				/* arrive au sol
				if(this.offSetY < 2)
				{
					this.jump = false;
					this.fall = false;
				}//*/
			}
		}
	}
	
	public void doBugfixEvent()
	{
		int errType = this.typeOfHoveredBlock(this.level.getSortedData());
		
		if(errType == this.level.ERR_WARNING)
		{
			
		}
		else if(errType == this.level.ERR_ERROR)
		{
			
		}
		else
		{
			
		}
		
	}
	
	public void draw(SpriteBatch sb, Level level)
	{
		if(ismove)
		{
			animation++;
			if(animation % 16 == 0){
				if(num_animation >= 3){
					num_animation = -1;
				}
				num_animation++;
			}
		}
		
		sb.draw(this.pxl, offSetX,offSetY,game.UNIT * 2, game.UNIT*2,game.UNIT*2 * num_animation ,0,32,32,false,false);
	}
	
	public void update(OrthographicCamera camera, Level level)
	{
		//this.x_unit = Math.round(this.offSetX / this.game.UNIT); //coordonnee x
		//this.y_unit = Math.round(this.offSetY / this.game.UNIT); //coordonnee y
		//this.num_case = 1500 - ((30 - (this.y_unit * 30)) + this.x_unit);
		
		int t = this.typeOfHoveredBlock(this.level.getSortedData());
		
		//System.out.println(num_case);
		this.Gravity(level);
		
		ismove = false;
		
		if(Gdx.input.isKeyPressed(Keys.SPACE))
		{
			this.x = game.UNIT * 25;
			this.y = game.UNIT * 25;
			this.offSetX = this.x;
			this.offSetY = this.y;
		}
		
		boolean inputLeft = Gdx.input.isKeyPressed(Keys.LEFT);
		boolean inputRight = Gdx.input.isKeyPressed(Keys.RIGHT);
		boolean inputUp = Gdx.input.isKeyPressed(Keys.UP);
		boolean deplacement = (inputLeft || inputRight || inputUp);
		
		boolean inputDown = Gdx.input.isKeyPressed(Keys.DOWN);
		
		if(deplacement)
		{
			
		}
		
		if(inputLeft)
		{
			this.moveLeft();
		}
		
		if(inputRight)
		{
			this.moveRight();
		}
		
		if(inputUp)
		{
			this.moveJump();
		}
		
		this.moveFall();
		
		//if(Gdx.input.isKeyPressed(Keys.UP)) this.offSetY += 200 * Gdx.graphics.getDeltaTime();
	}

	public void Gravity(Level level)
	{
		//System.out.println(this.num_case +49);
		//*
		int thenewY = Math.round(this.offSetY - this.gravity * Gdx.graphics.getDeltaTime());
		if(!isCollisionY(this.blocks, thenewY))
		{
			this.offSetY -= this.gravity * Gdx.graphics.getDeltaTime();
			this.y -= this.gravity * Gdx.graphics.getDeltaTime();	
		}
		else
		{
			this.jump = false;
			this.fall = false;
		}
		//*/
	}
}
