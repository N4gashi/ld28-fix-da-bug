package my.ld.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Json;

public class Level
{
	private int[] data;
	private MyGame game;
	private Texture red;
	private ArrayList<Block> blocks;
	private int nblines;
	private int nbcases;
	
	public final int ERR_WARNING = 2;
	public final int ERR_ERROR = 3;
	
	public Level()
	{
		this.red = new Texture(Gdx.files.internal("assets/red.png"));
		this.blocks = new ArrayList<Block>();
	}
	
	public int[] getData()
	{
		return this.data;
	}
	
	public void setData()
	{
		//this.data = the_data;
	}
	
	public ArrayList<Block> getBlocks()
	{
		return this.blocks;
	}
	
	public int[][] getSortedData()
	{
		
		int lines = (this.game.RES_HEIGHT / this.game.UNIT);
		int valuesPerLines = (this.game.RES_WIDTH / this.game.UNIT);
		
		lines = this.nblines;
		valuesPerLines = this.nbcases;
	
		//System.out.println(lines);
		
		int[][] sortedData = new int[lines][valuesPerLines];
		int numeroLigne = 0;
		int numeroIndex = 0;
		
		for(int i=0; i<(this.data.length); i++)
		{
			//System.out.println(sortedData[0][0]);
			//System.out.println(lines + "|" + valuesPerLines + " data / " + sortedData.length);
			
			//System.out.println("sortedData["+numeroLigne+"]["+numeroIndex+"] = " + data[i]);
			sortedData[numeroLigne][numeroIndex] = data[i];
			//System.out.println("sortedData[numeroLigne]["+numeroIndex+"] = data[i];");
			//System.out.println("sortedData["+numeroLigne+"]["+numeroIndex+"] = "+sortedData[numeroLigne][numeroIndex]);
			numeroIndex++;
			
			
			if((i % (valuesPerLines)) == 0)
			{
				//System.out.println(numeroLigne);
				if(i != 0 && numeroLigne < (valuesPerLines-1))
					numeroLigne++;
				
				numeroIndex = 0;
			}
			
		}
		
		//System.out.println("sortedData.length !!!!!" + sortedData.length);
		
		int[][] tempTab = new int[lines][valuesPerLines];
		int cpt = 0;
		
		
		for(int currentCase=(tempTab.length-1); currentCase >= 0; currentCase--)
		{
			// currentCase = 
			//System.out.println("tempTab.length-1 : " + (tempTab.length-1));
			//System.out.println("currentCase : " + currentCase);
			//System.out.println(i);
			// i(1) => 49
			//System.out.println("tempTab[i] = sortedData[currentCase];");
			//System.out.println("tempTab["+cpt+"] = sortedData["+currentCase+"];");
			tempTab[cpt] = sortedData[currentCase];
			cpt++;
		}
		
		sortedData = tempTab;
		
		/*for(int currentLine=0; currentLine < sortedData.length; currentLine++)
		{
			
			
			// Currentline = 0
			// Sorteddata.length = 30
			
			// sortedData[currentLine].length => sortedData[0].length
			
			//System.out.println(tempTab.length);
			
			
			sortedData[currentLine] = tempTab[currentLine];
		}*/
		
		numeroLigne = 0;
		numeroIndex = 0;
		
		for(int i=0; i<(this.data.length-29); i++)
		{
			
			//sortedData[numeroLigne][numeroIndex] = data[i];
			//System.out.println(lines + "|" + valuesPerLines + " data / " + sortedData.length);
			//System.out.println("sortedData["+numeroLigne+"]["+numeroIndex+"] = " + sortedData[numeroLigne][numeroIndex]);
			//System.out.println("sortedData["+numeroLigne+"]["+numeroIndex+"] = "+sortedData[numeroLigne][numeroIndex]);
			numeroIndex++;
			
			if((i % (valuesPerLines-1)) == 0 && (i != 0))
			{
				numeroLigne++;
				numeroIndex = 0;
			}
		}
		return sortedData;
	}
	
	public void setGameContext(MyGame game)
	{
		this.game = game;
	}
	
	public void printData()
	{
		// VOILA
		this.getSortedData();
		//System.out.println();
	}

	public void draw(SpriteBatch sb)
	{
		
		for(Block block : this.blocks)
		{
			block.draw(sb);
		}
	}

	public void update(Player player)
	{
		int[][] thedata = this.getSortedData();
		
		for(int i = 0; i < (thedata.length-1); i++)
		{
			for(int j=0; j < thedata[i].length; j++)
			{
				
				if(thedata[i][j] != 0)
				{
					// i => ligne en cours (y)
					// j => x
					//System.out.println(" x : " + (j) + " y :" +(i) + "width :" +game.UNIT + "height :" + game.UNIT);
					//break;
					//this.blocks.add(new Block(game, red, (float)(j*game.UNIT), (float)(i*game.UNIT)));
					
					//new Rectangle((float)this.offSetX, (float)this.offSetY, (float)game.UNIT*2, (float)this.game.UNIT*2);
					//sb.draw(this.red, (game.UNIT * j), (game.UNIT * i), game.UNIT, game.UNIT);
				}
			}
		}
		
		System.out.println();
	}

}
