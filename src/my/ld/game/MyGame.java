package my.ld.game;

import GameScreens.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyGame extends Game {
	
	
	public final int UNIT = 8;
	final int RES_HEIGHT = 480;
	final int RES_WIDTH = 800;
    public MainScreen mainScreen;
    public IntroScreen introScreen;
    public GameScreen gameScreen;
    
    public SpriteBatch batch;

   @Override
    public void create() {
	   batch = new SpriteBatch();
	   //dropImage = new Texture(Gdx.files.internal("droplet.png"));
	   
            mainScreen = new MainScreen(this);
            introScreen = new IntroScreen(this);
            gameScreen = new GameScreen(this);
            setScreen(mainScreen);              
    }
}