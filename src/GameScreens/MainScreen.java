package GameScreens;

import my.ld.game.MyGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MainScreen implements Screen {
 

       MyGame game; // Note it's "MyGame" not "Game"
       private  Texture pxl;
       private SpriteBatch spriteBatch;
       
       // constructor to keep a reference to the main Game class
        public MainScreen(MyGame game){
        	this.spriteBatch = new SpriteBatch();
            this.game = game;
            
      
        }
        
        @Override
        public void render(float delta) {
        	//--------------UPDATE--------------//
            if (Gdx.input.isKeyPressed(Keys.ENTER)) // use your own criterion here
            	game.setScreen(game.introScreen);
            
            
            //----------------DRAW--------------//
            Gdx.gl.glClearColor(255,0,0,1);
            Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
            
        }
 

       @Override
        public void resize(int width, int height) {
        }
 

       @Override
        public void show() {
             // called when this screen is set as the screen with game.setScreen();
        }
 

       @Override
        public void hide() {
             // called when current screen changes from this to a different screen
        }
 

       @Override
        public void pause() {
        }
 

       @Override
        public void resume() {
        }
 

       @Override
        public void dispose()
       	{
                // never called automatically
        }
 }