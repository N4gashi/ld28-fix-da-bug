package GameScreens;

import my.ld.game.MyGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;

public class IntroScreen implements Screen {
 

       MyGame game; // Note it's "MyGame" not "Game"
 

       // constructor to keep a reference to the main Game class
        public IntroScreen(MyGame game){
                this.game = game;
        }
        

        @Override
        public void render(float delta) {
                // update and draw stuff
        	if (Gdx.input.isKeyPressed(Keys.ENTER)) // use your own criterion here
            	game.setScreen(game.gameScreen);
        	Gdx.gl.glClearColor(0,255,0,1);
            Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        }
 

       @Override
        public void resize(int width, int height) {
        }
 

       @Override
        public void show() {
             // called when this screen is set as the screen with game.setScreen();
        }
 

       @Override
        public void hide() {
             // called when current screen changes from this to a different screen
        }
 

       @Override
        public void pause() {
        }
 

       @Override
        public void resume() {
        }
 

       @Override
        public void dispose() {
                // never called automatically
        }
 }
