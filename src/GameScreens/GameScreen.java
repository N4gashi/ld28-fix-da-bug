package GameScreens;

import java.io.Console;
import java.io.File;
import java.util.ArrayList;

import my.ld.game.Block;
import my.ld.game.Level;
import my.ld.game.Main;
import my.ld.game.MyGame;
import my.ld.game.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Json;

public class GameScreen implements Screen {

       MyGame game; // Note it's "MyGame" not "Game"
       private OrthographicCamera camera;
       private Player player;
       private SpriteBatch spriteBatch;
       private Texture fond;
       private Level level;
       
       // constructor to keep a reference to the main Game class
        public GameScreen(MyGame game){
                this.game = game;
                Texture.setEnforcePotImages(false);
                this.spriteBatch = new SpriteBatch();
                this.fond = new Texture(Gdx.files.internal("assets/maptiled.png"));
                camera = new OrthographicCamera();
        		camera.setToOrtho(false, 800, 480);
        		
        		
        		
        		// CHARGER LE LEVEL
        		FileHandle mapFile = new FileHandle(new File("assets/maptiled.json"));
        		this.level = this.loadLevelFromJSON(mapFile);
        		this.level.setGameContext(this.game);
        		
        		this.level.printData();
        		
        		
        		this.player = new Player(game, level);
        }
        
        @Override
        public void render(float delta) {
                // update and draw stuff
        	//if (Gdx.input.isKeyPressed(Keys.ENTER)) // use your own criterion here
            	//game.setScreen(game.gameScreen);
        	Gdx.gl.glClearColor(0,0,0,1);
            Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
            
            player.update(this.camera, this.level);
            camera.update();
            level.update(this.player);
            
            spriteBatch.begin();
	            spriteBatch.draw(this.fond,0, 0);
	            player.draw(spriteBatch, this.level);
	            this.level.draw(spriteBatch);
            spriteBatch.end();
            
            
        }
 

       @Override
        public void resize(int width, int height) {
        }
 

       @Override
        public void show() {
             // called when this screen is set as the screen with game.setScreen();
        }
 

       @Override
        public void hide() {
             // called when current screen changes from this to a different screen
        }
 

       @Override
        public void pause() {
        }
 

       @Override
        public void resume() {
        }
 

       @Override
        public void dispose() {
                // never called automatically
    	   
    	   
    	   
        }
       
       public Level loadLevelFromJSON(FileHandle file)
       {
    	    Json json = new Json();
   			Level loadedLevel = (Level)json.fromJson(Level.class, file);
   			return loadedLevel;
       }
 }
